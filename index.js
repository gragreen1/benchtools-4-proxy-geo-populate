const https = require('https');
const mysql = require('mysql');
const axios = require('axios');
const dns = require('dns');

const connection = mysql.createConnection({
  host: '35.232.103.16',
  user: 'bt-webuser',
  password: '',
  database: 'benchtools'
});

connection.connect((err) => {
	if (err) {
		console.error(err);
	};
});

const getSqlQuery = async (baseAddress, id, host) => {
	let result;
	axios.get(`https://ipapi.co/${baseAddress}/json/`).then(response => {
		let d = response.data;
		result = `UPDATE proxies p SET p.latitude="${d.latitude}", p.longitude="${d.longitude}", p.region="${d.region}", p.city="${d.city}" WHERE p.id=${id} AND p.host='${host}';`;
		console.log(result);
	}).catch(function(err) {
		console.error(err);
	});
};

const eachRow = async (row) => {
	let baseAddress = row.host.split(":")[0];
	let rowResult;
	dns.lookup(baseAddress, async (err, addr, family) => {
		if (addr) {
			rowResult = await getSqlQuery(addr, row.id, row.host);
		}
	});
	return rowResult;
};



const run = async (connection) => {
	let runResult;
	connection.query(`SELECT * FROM proxies p WHERE p.latitude IS NULL AND p.longitude IS NULL`, async (err, rows) => {
		for (const row of rows) {
			// Avoid HTTP 429 "Too Many Requests" responses by simple throttling timeout call.
			await new Promise(r => setTimeout(r, 2000));
			await eachRow(row);
		}
	}).catch(function(err) {
		console.error(err);
	});
};

run(connection);
